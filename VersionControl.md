% Version Control
% Victor Sluiter
% 2016-06-14


# Version Control

## What is version control?

- Keep track of history of sets of files
- Collaborate on files
    - *multiple people working on one file*
    - *multiple people working on complete directories*
- Keep track of version of many files within a project
    - *which settings file belonged to which version of the experiment software?*
- [This presentation](https://bitbucket.org/VIJSluiter/version-control-presentation)

## Why was it created?

- Programmers need to change many things in many (code) files
- Need to "version" systems. Track origin of bugs!
- Zip files tend to get messy....
- Collaboration with multiple programmers
- Backup

## How it works:

- Centralized server with code / files &rarr; **`Remote Repository`**
- You can get code from the server &rarr; **'Pull'**
- You can make changes to the code. When you want to save a version, you save the current *state of all files* in the **`Local Repository`** &rarr; **`commit`**
- You can **`commit`** as often as you like. More = better!
- You can put your changed version back on the **'Remote Repository'** &rarr; **'Push'**

## With multiple people:

- Version Control will see that others have made changes too
- If files are text based &rarr; **`automatic merge`**
- If it's too hard for automatic, you can **`diff`** and merge yourself.

## Is there one industry standard....?

## No.

Different flavours:

- Subversion
    - Sarthak's group. 'Older' system
- Mercurial
    - Gijs' prefered solution. Easier to use.
- Git
    - Most common system. Harder to use.

Are most popular

# Why Version Control in Academic use?

## Short answer:

![](images/phd101212s_600.png)

## Longer answer:

- What were the settings you used for the experiment last year?
- How can you share your code with a colleague **and** both keep up-to-date-versions?
- If someone finds a bug in your code you wrote last year, can you check when you fixed it yourself?
- Can you collaborate over multiple locations?
- If your software / LaTeX project fails, how much work is lost when recovering?
- You'll want to do better than `thesis_final_final_v2_with_comments.docx`

# Workflow

##  Basic Version Control
- **`Pull`** a version from a server
- Edit code
- **`commit`** your changes with good description. Do this often!
- **`Push`** the modified version back to server

## Why commit messages?

- Keep track of what you did  
- See **why** you changed that line / paragraph / ...
- Be able to judge when that nasty bug popped up.

![](images/commits.png)

## This is boring ;)

![](images/git_commit.png)

## But very useful when bugtracking

Try to find when a new function was added:

![](images/bad_commits.png)

Luckily you can *diff*

# Extras

## Branches

- A bit confusing in Git, but great concept.
    - Have a new idea? Try it without messing up original version!!
- Allows people to work on their own section

![](images/branches.png)

## Tags

Create a label to show which version is important.
![](images/commits.png)

# But.....

## But... We've got Dropbox!

- Dropbox does not allow version control of complete directories
- Mac and Windows and Linux line endings mess up
- No commit messages, so no clue what was changed by whom!
- No way to branch; need to make zips again...
- How do you give someone the right to edit, but preserve your own versions?
- [Stackexchange....](http://programmers.stackexchange.com/questions/170791/dropbox-as-a-version-control-tool)

## But... I don't collaborate!

- Working with students?
- I keep track of personal projects with Git for ease of bugfixing, version control and backup.
- Git can work on a local PC. I often start out locally, if code is useful, I push it to a server.

## But... I'm working with binary files

- No 'diff' of binaries
- but still sharing and collaborating
- ...and commits, and branches.....

## But... Version control is a PITA!

- Yes... You have to learn it...
- [Bitbucket Tutorials](https://confluence.atlassian.com/bitbucket/bitbucket-tutorials-teams-in-space-training-ground-755338051.html)
- [Git Immersion](http://gitimmersion.com)
- [Code Academy Git](https://www.codecademy.com/learn/learn-git)
- [Set up Simulink with Git](http://nl.mathworks.com/help/simulink/ug/set-up-git-source-control.html?requestedDomain=www.mathworks.com)
- [Learn Git Branching (interactive)](http://pcottle.github.io/learnGitBranching/)
-[Git cheatsheet](http://ndpsoftware.com/git-cheatsheet.html)

For GIT: originally pure Linux, so lots of command line. Now for windows: SourceTree from Bitbucket.

# OK. You got me. How do I start?

## Bitbucket

- [https://bitbucket.org/ctw-bw/](https://bitbucket.org/ctw-bw/)
- Repositories can be made **`private`**!
- Provides storage (*"Dropbox"*) and versioning
- You can give access to your repository to people from outside UT.
- Our academic acount is unlimited in users and storage
- Gijs and I are admins and can add you to the group.

## Bonus Features of Bitbucket

- [Issue tracker](https://bitbucket.org/ctw-bw/symbitron-we-electronics/issues)
- [Wiki](https://bitbucket.org/ctw-bw/symbitron-we-electronics/wiki/Motor%20test.md)

## Who's using it?

- Symbitron:
    - Technical documentation
    - All EtherLab/Simulink software, across Europe
- BioMechTools
    - Sharing and collaborating on ultrasound project
    - Kenan makes DLLs + LabView example, I use same functions in 'main' program
- Gijs' LopesInterface
    - Again, easy to share, easy to share changes
- ArmWorks
    - Share common code

# Summary
## Summary

- Do some Tutorials
- Start off with a local version controlled project
- Learn yourself to commit **often**!
- Push it to the server
- ... Solve some nasty issues with detached heads.....
- And still be happy to have version control instead of zips!

## Workshop?

- Common issues and how to solve them
- Branches, stashes and submodules  
![](images/git_xkcd.png)

# Bad Code vs Good Code

## Bad Code vs Good Code

![](images/wtfm.jpg)
